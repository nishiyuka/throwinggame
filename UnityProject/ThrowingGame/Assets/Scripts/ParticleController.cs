using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartParticle(Vector3 createPosition)
    {
        ParticleSystem newParticle = Instantiate(particle);
        newParticle.transform.position = createPosition;
        newParticle.Play();
        GetComponent<AudioSource>().Play();
        Destroy(newParticle.gameObject, 5.0f);
    }
}
