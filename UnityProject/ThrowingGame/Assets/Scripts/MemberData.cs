
public class MemberData
{
    public string Name { get; set; }
    public int Score { get; set; }

    public MemberData()
    {
        Name = "";
        Score = 0;
    }
}
