using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{
    public GameObject aimSlider;
    public GameManager GameManager;

    private float speed = 25.0f;
    private float horizuntalturnvalue = 0.0f;
    private float verticalturnvalue = 0.0f;
    private float turnspeed = 20.0f;
    private float maxturn = 45.0f;
    private float minturn = 0.0f;
    private Rigidbody rb;
    private bool isBottunDown = false;
    private bool isLeftButtonDown = false;
    private bool isRightButtonDown = false;
    private bool isUpButtonDown = false;
    private bool isDownButtonDown = false;
    private float sliderMinValue = 25.0f;
    private float speedMaxValue = 80.0f;
    private Slider slider;
    private Vector3 localPosition;
    private Vector3 localAngle;
    


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        aimSlider.SetActive(true);
        slider = aimSlider.GetComponent<Slider>();
        slider.value = sliderMinValue;
        localPosition = transform.position;
        localAngle = transform.localEulerAngles;
        

    }

    // Update is called once per frame
    void Update()
    {
        //投げた後的に当たらず地面の外に落ちた時またはz座標の20より奥に行った時
        if ((transform.position.y < 1.0f) || (transform.position.z > 20.0f))
        {
            this.gameObject.SetActive(false);
            GameManager.AddScore(0);
        }
        //投げるボタン
        if (isBottunDown)
        {
            speed += Time.deltaTime * 100;
            if (speed > speedMaxValue)
            {
                speed = speedMaxValue;
            }
            slider.value = sliderMinValue + speed;
        }
        //左回りのボタン
        if (isLeftButtonDown)
        {
            horizuntalturnvalue += Time.deltaTime * turnspeed * -1 ;
            if (maxturn * -1 > horizuntalturnvalue)
            {
                horizuntalturnvalue = maxturn * -1;
            }
            Quaternion turnRotation = Quaternion.Euler(verticalturnvalue, horizuntalturnvalue, 0f);
            rb.MoveRotation(turnRotation);
        }
        //右回りのボタン
        if (isRightButtonDown)
        {
            horizuntalturnvalue += Time.deltaTime * turnspeed;
            if (maxturn < horizuntalturnvalue)
            {
                horizuntalturnvalue = maxturn;
            }
            Quaternion turnRotation = Quaternion.Euler(verticalturnvalue, horizuntalturnvalue, 0f);
            rb.MoveRotation(turnRotation);
        }
        //上向きのボタン
        if (isUpButtonDown)
        {
            verticalturnvalue += Time.deltaTime * turnspeed * -1;
            if (maxturn * -1 > verticalturnvalue)
            {
                verticalturnvalue = maxturn * -1;
            }
            Quaternion turnRotation = Quaternion.Euler(verticalturnvalue, horizuntalturnvalue, 0f);
            rb.MoveRotation(turnRotation);
        }
        //下向きのボタン
        if (isDownButtonDown)
        {
            verticalturnvalue += Time.deltaTime * turnspeed;
            if (minturn < verticalturnvalue)
            {
                verticalturnvalue = minturn;
            }
            Quaternion turnRotation = Quaternion.Euler(verticalturnvalue, horizuntalturnvalue, 0f);
            rb.MoveRotation(turnRotation);
        }
    }

    //投げるボタンの処理
    public void PointerDown()
    {
        isBottunDown = true;
    }
    public void PointerUp()
    {
        isBottunDown = false;
        aimSlider.SetActive(false);
        rb.AddRelativeForce(Vector3.forward * speed, ForceMode.Impulse);
        speed = 0.0f;
    }

    //左回りのボタンの処理
    public void LeftPointerDown()
    {
        isLeftButtonDown = true;
    }
    //右回りのボタンの処理
    public void RightPointerDown()
    {
        isRightButtonDown = true;
    }

    //上向きのボタンの処理
    public void UpPointerDown()
    {
        isUpButtonDown = true;
    }

    //下向きのボタンの処理
    public void DownPointerDown()
    {
        isDownButtonDown = true;
    }

    //回転ボタン全般の処理
    public void rotatePointerUp()
    {
        isLeftButtonDown = false;
        isRightButtonDown = false;
        isUpButtonDown = false;
        isDownButtonDown = false;
    }
    public void Reset()
    {
        transform.position = localPosition;//myTransform
        transform.localEulerAngles = localAngle;//myTransform
        aimSlider.SetActive(true);
        slider.value = sliderMinValue;
        horizuntalturnvalue = 0.0f;
        verticalturnvalue = 0.0f;
    }

}
