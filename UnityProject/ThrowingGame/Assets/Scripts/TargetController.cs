using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TargetController : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject particleController;
    private Dictionary<string, int> scoreDictionary;
    private string targetScoreKey;

    // Start is called before the first frame update
    void Start()
    {
        if (tag == "TargetS")
        {
            transform.DOMove(new Vector3(0, 0, 20f), 5f).SetRelative(true).SetLoops(-1, LoopType.Yoyo);
        }
        else if (tag == "TargetM")
        {
            transform.DOMove(new Vector3(0, 0, 15f), 4f).SetRelative(true).SetLoops(-1, LoopType.Yoyo);
        }
        else if (tag == "TargetL")
        {
            transform.DOMove(new Vector3(0, 0, 10f), 3f).SetRelative(true).SetLoops(-1, LoopType.Yoyo);
        }
        
        scoreDictionary = new Dictionary<string, int>()
        {
            {"S", 300},
            {"M", 200},
            {"L", 100}
        };
    }

    // Update is called once per frame
    void Update()
    {
        if ((gameManager.GetComponent<GameManager>().isOnClickRestart) || (gameManager.GetComponent<GameManager>().isOnClickEnd))
        {
            this.transform.DOKill();
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        if (tag == "TargetS")
        {
            targetScoreKey = "S";
        }
        else if (tag == "TargetM")
        {
            targetScoreKey = "M";
        }
        else if (tag == "TargetL")
        {
            targetScoreKey = "L";
        }

        particleController.GetComponent<ParticleController>().StartParticle(this.transform.position);
        other.gameObject.SetActive(false);
        this.transform.DOKill();
        this.gameObject.SetActive(false);
        gameManager.AddScore(scoreDictionary[targetScoreKey]);
    }
    
}
