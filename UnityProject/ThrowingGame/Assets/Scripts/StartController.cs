using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System.Linq;



public class StartController : MonoBehaviour
{
    public static string Name;
    public GameObject InputField;
    public GameObject EntryButton;
    public GameObject StartButton;
    public GameObject MessageText;

    private Text messagetext;
    private string[] InvalidChars = {" ", "　","\\","。"}; 
    
    // Start is called before the first frame update
    void Start()
    {
        Name = "";
        StartButton.SetActive(true);
        InputField.SetActive(false);
        EntryButton.SetActive(false);
        MessageText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnClickEntryButton()
    {
        Name = InputField.GetComponent<InputField>().text;
        if (!(Name == ""))
        {
            SceneManager.LoadScene("MainScene");
        }
    }

    public void OnClickStartButton()
    {
        StartButton.SetActive(false);
        MessageText.SetActive(true);
        StartCoroutine(SetName());
    }

    private IEnumerator SetName()
    {
        string beforeText = ""; // messagetext.text;
        messagetext = MessageText.GetComponent<Text>();
        messagetext.DOText("Throwing Gameへようこそ\n名前を入力してください。", 5f)
            .SetEase(Ease.Linear)
            .OnUpdate(() =>
            {
                string currentText = messagetext.text;
                if (beforeText == currentText)
                {
                    return;
                }
                string newChar = currentText[currentText.Length - 1].ToString();
                if (!InvalidChars.Contains(newChar))
                {
                    GetComponent<AudioSource>().Play();
                }
                beforeText = currentText;
            });
        yield return new WaitForSeconds(3);
        InputField.SetActive(true);
        EntryButton.SetActive(true);
    }

}
