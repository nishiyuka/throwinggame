using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;
using System;

public class MemberDataModel
{
    public static List<MemberData> DeserializeFromJson(string sStrJson)
    {
        var ret = new List<MemberData>();
        IList jsonList = (IList)Json.Deserialize(sStrJson);
        foreach (IDictionary jsonOne in jsonList)
        {
            var tmp = new MemberData();
            if (jsonOne.Contains("name"))
            {
                tmp.Name = (string)jsonOne["name"];
            }
            if (jsonOne.Contains("score"))
            {
                tmp.Score = (int)(long)jsonOne["score"];
            }
            ret.Add(tmp);
        }
        return ret;
    }
}
