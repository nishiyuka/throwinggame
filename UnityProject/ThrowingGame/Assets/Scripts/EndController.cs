using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using DG.Tweening;

public class EndController : MonoBehaviour
{
    public GameObject TitleText;
    public GameObject TextField;
    public GameObject ScoreField;
    public GameObject EndText;

    private List<MemberData> memberList;
    private float time = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SetTitleText());
    }

    private IEnumerator SetTitleText()
    {
        TitleText.GetComponent<Text>().DOText("ランキング", 2f);
        yield return new WaitForSeconds(3);
        TextField.GetComponent<Text>().text = "Wait...";
        ScoreField.GetComponent<Text>().text = "";
        SetJsonFromWWW();
    }


    private void SetJsonFromWWW()
    {
        string setURL = "http://localhost/throwingapi/throwing/setThrowing";
        string name = StartController.Name;
        string score = GameManager.totalScore.ToString();
        StartCoroutine(SetMessage(setURL, name, score, WebRequestSuccess, CallbackWebRequestFailed));
    }

    private IEnumerator SetMessage(string url, string name, string score, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("score", score);
        UnityWebRequest webRequest = UnityWebRequest.Post(url, form);
        webRequest.timeout = 5;
        yield return webRequest.SendWebRequest();
        //Debug.Log($"Success:{webRequest.downloadHandler.text }");
        if (webRequest.error != null)
        {
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            //Debug.Log($"Success:{webRequest.downloadHandler.text }");
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }

    private void WebRequestSuccess(string response)
    {
        GetJsonFromWebRequest();
    }

    private void CallbackWebRequestFailed()
    {
        TextField.GetComponent<Text>().text = "WWW Faild";
    }

    private void GetJsonFromWebRequest()
    {
        StartCoroutine(DownloadJson(CallbackWebRequestSuccess, CallbackWebRequestFailed));
    }

    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/throwingapi/throwing/getThrowings");
        yield return www.SendWebRequest();
        if (www.error != null)
        {
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (www.isDone)
        {
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }

    private void CallbackWebRequestSuccess(string response)
    {
        memberList = MemberDataModel.DeserializeFromJson(response);
        string sStrOutputName = "";
        string sStrOutputScore = "";
        if (null == memberList)
        {
            TextField.GetComponent<Text>().text = "no list";
        }
        else
        {
            foreach (MemberData memberOne in memberList)
            {
                sStrOutputName += $"name:{memberOne.Name} \n";
                sStrOutputScore += $"score:{memberOne.Score} \n";
            }
        }
        TextField.GetComponent<Text>().text = sStrOutputName;
        ScoreField.GetComponent<Text>().text = sStrOutputScore;
        EndText.GetComponent<Text>().DOText("End. Thanks for playing!", 3f);
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime * 0.01f;
        GetComponent<AudioSource>().volume = 0.1f - time;
    }
}
