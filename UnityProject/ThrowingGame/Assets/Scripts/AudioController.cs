using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public GameManager GameManager;
    private float time = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSound());
    }
    private IEnumerator StartSound()
    {
        yield return new WaitForSeconds(2.6f);
        GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.GetComponent<GameManager>().isOnSound == true)
        {
            if (GameManager.GetComponent<GameManager>().isEnd == true)
            {
                time += Time.deltaTime * 0.03f;
                GetComponent<AudioSource>().volume = 0.1f - time;
            }
            else
            {
                GetComponent<AudioSource>().volume = 0.1f;
            }
        }
        else
        {
            GetComponent<AudioSource>().volume = 0f;
        }

    }
}
