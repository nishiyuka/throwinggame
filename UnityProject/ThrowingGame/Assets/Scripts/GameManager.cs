using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManager : MonoBehaviour
{

    public static int totalScore;
    public GameObject Ball;
    public GameObject BallCountText;
    public GameObject ScoreCountText;
    public GameObject GameOverText;
    public GameObject RestartButton;
    public GameObject EndButton;
    public GameObject ScoreMessageText;
    public RectTransform UpShutter;
    public RectTransform DownShutter;


    private int ballCount = 0;
    private int maxBallCount = 4;

    public bool isEnd = false;

    public bool isOnClickRestart = false;
    public bool isOnClickEnd = false;
    public bool isOnSound = false;
    

    // Start is called before the first frame update
    void Start()
    {
        totalScore = 0;
        GameOverText.GetComponent<Text>().text = null;
        RestartButton.SetActive(false);
        EndButton.SetActive(false);
        StartCoroutine(OpenShutter());
    }
    private IEnumerator OpenShutter()
    {
        UpShutter.DOAnchorPos(new Vector2(0, 810), 2.6f);
        DownShutter.DOAnchorPos(new Vector2(0, -810), 2.6f);
        StartCoroutine(ShutterSound());
        yield return new WaitForSeconds(2.6f);
        isOnSound = true;
    }
    private IEnumerator ShutterSound()
    {
        yield return new WaitForSeconds(0.05f);
        GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        SetText();
        if (isEnd)
        {
            GameOverText.GetComponent<Text>().text = "GAME OVER!!!";
            RestartButton.SetActive(true);
            EndButton.SetActive(true);
        }
        if (isOnClickRestart)
        {
            int sceneIndex = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(sceneIndex);
        }
        if (isOnClickEnd)
        {
            int sceneIndex = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(sceneIndex + 1);
        }
    }
    public void AddScore(int score)
    {
        totalScore += score;
        ballReset();
    }
    private void SetText()
    {
        ScoreMessageText.GetComponent<Text>().text = $"{StartController.Name} さんのスコア";
        ScoreCountText.GetComponent<Text>().text = "スコア　　" + totalScore + " 点";
        BallCountText.GetComponent<Text>().text =  "あと　　　" + (maxBallCount + 1 - ballCount) + " 回";
    }
    private void ballReset()
    {
        if (ballCount == maxBallCount)
        {
            isEnd = true;
            ballCount += 1;
        }
        else
        {
            ballCount += 1;
            Ball.SetActive(true);           
            Ball.GetComponent<Rigidbody>().isKinematic = true;
            Ball.GetComponent<BallController>().Reset();
            Ball.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
    public void OnClickRestartButton()
    {
        isOnSound = false;
        StartCoroutine(CloseShutter("restart"));
    }
    public void OnClickEndButton()
    {
        isOnSound = false;
        StartCoroutine(CloseShutter("end"));
    }
    private IEnumerator CloseShutter(string result)
    {
        UpShutter.DOAnchorPos(new Vector2(0, 270), 2.6f);
        DownShutter.DOAnchorPos(new Vector2(0, -270), 2.6f);
        StartCoroutine(ShutterSound());
        yield return new WaitForSeconds(2.6f);
        if (result == "restart")
        {
            isOnClickRestart = true;
        }
        if (result == "end")
        {
            isOnClickEnd = true;
        }
    }
}
