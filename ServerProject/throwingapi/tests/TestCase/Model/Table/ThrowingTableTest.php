<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ThrowingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ThrowingTable Test Case
 */
class ThrowingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ThrowingTable
     */
    public $Throwing;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.throwing'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Throwing') ? [] : ['className' => ThrowingTable::class];
        $this->Throwing = TableRegistry::getTableLocator()->get('Throwing', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Throwing);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
