<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Throwing $throwing
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Throwing'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="throwing form large-9 medium-8 columns content">
    <?= $this->Form->create($throwing) ?>
    <fieldset>
        <legend><?= __('Add Throwing') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('score');
            echo $this->Form->control('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
