<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Throwing $throwing
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Throwing'), ['action' => 'edit', $throwing->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Throwing'), ['action' => 'delete', $throwing->id], ['confirm' => __('Are you sure you want to delete # {0}?', $throwing->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Throwing'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Throwing'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="throwing view large-9 medium-8 columns content">
    <h3><?= h($throwing->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($throwing->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($throwing->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($throwing->score) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($throwing->date) ?></td>
        </tr>
    </table>
</div>
