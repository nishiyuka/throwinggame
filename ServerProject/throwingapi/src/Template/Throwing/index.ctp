<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Throwing[]|\Cake\Collection\CollectionInterface $throwing
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Throwing'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="throwing index large-9 medium-8 columns content">
    <h3><?= __('Throwing') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('score') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($throwing as $throwing): ?>
            <tr>
                <td><?= $this->Number->format($throwing->id) ?></td>
                <td><?= h($throwing->name) ?></td>
                <td><?= $this->Number->format($throwing->score) ?></td>
                <td><?= h($throwing->date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $throwing->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $throwing->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $throwing->id], ['confirm' => __('Are you sure you want to delete # {0}?', $throwing->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
