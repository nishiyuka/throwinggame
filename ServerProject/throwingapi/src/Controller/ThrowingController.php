<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Throwing Controller
 *
 * @property \App\Model\Table\ThrowingTable $Throwing
 *
 * @method \App\Model\Entity\Throwing[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ThrowingController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $throwing = $this->paginate($this->Throwing);

        $this->set(compact('throwing'));
    }

    /**
     * View method
     *
     * @param string|null $id Throwing id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $throwing = $this->Throwing->get($id, [
            'contain' => []
        ]);

        $this->set('throwing', $throwing);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $throwing = $this->Throwing->newEntity();
        if ($this->request->is('post')) {
            $throwing = $this->Throwing->patchEntity($throwing, $this->request->getData());
            if ($this->Throwing->save($throwing)) {
                $this->Flash->success(__('The throwing has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The throwing could not be saved. Please, try again.'));
        }
        $this->set(compact('throwing'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Throwing id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $throwing = $this->Throwing->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $throwing = $this->Throwing->patchEntity($throwing, $this->request->getData());
            if ($this->Throwing->save($throwing)) {
                $this->Flash->success(__('The throwing has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The throwing could not be saved. Please, try again.'));
        }
        $this->set(compact('throwing'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Throwing id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $throwing = $this->Throwing->get($id);
        if ($this->Throwing->delete($throwing)) {
            $this->Flash->success(__('The throwing has been deleted.'));
        } else {
            $this->Flash->error(__('The throwing could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //Throwingテーブルから上位10件までのランキングデータを降順で取得
    public function getThrowings()
    {
        $this->autoRender = false;
        $query = $this->Throwing->find("all");
        $query->order(['score'=>'DESC']);
        $query->limit(10);
        $json = json_encode($query);
        echo $json;
    }

    //Throwingテーブルに今回postされたデータを1レコード追加
    public function setThrowing()
    {
        $this->autoRender = false;
        $postName = $this->request->data["name"];
        $postScore = $this->request->data["score"];
        $record = array(
            "name"=>$postName,
            "score"=>$postScore,
            "date"=>date("Y/m/d H:i:s")
        );
        $prm1 = $this->Throwing->newEntity();
        $prm2 = $this->Throwing->patchEntity($prm1,$record);
        if ($this->Throwing->save($prm2)){
            echo "success";
        }else{
            echo "failed";
        }

    }
}
